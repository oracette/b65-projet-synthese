class Tile:
    def __init__(self, x, y, size):
        self.xPos = x
        self.yPos = y
        self.size = size
        self.bgEntity = None
        self.entities = []
        self.isOccupied = False
        self.isGround = False
        self.xTile = int(x / size)
        self.yTile = int(y / size)

        #A tile itself does not have a sprite. It can have an entity on it, which happens to have a sprite.
        #A tile can have multiple entities on it. It can also have an entity from the background (walls).
        #Foreground entities are not part of the tilemap. They serve a purely cosmetic purpose and cannot be interacted with by any other entity.