#Du a la maniere que pyglet fonctionne, la vue et le controleur sont ensemble. La classe Game et GameWindow sont maintenant dans une même classe, Game.
import GlobalTick
import GlobalId
import math
import copy
import pyglet

from Map import Map
from Level import Level
from Entities import *
from Camera import Camera
from pyglet.gl import *

from maps import *

class Game(pyglet.window.Window):
    def __init__(self):
        super(Game,self).__init__()
        GlobalTick.value = 0
        GlobalId.value = 0
        self.clock = pyglet.clock
        self.clockRate = 30 #rate in fps
        self.gameWidth = 1000
        self.gameHeight = 900
        self.tileSize = 32
        self.set_size(self.gameWidth, self.gameHeight)
        self.set_caption("RJ-2D")

        self.keys = pyglet.window.key.KeyStateHandler()
        self.push_handlers(self.keys)

        self.controls = {
            'left': pyglet.window.key.A,
            'right': pyglet.window.key.D,
            'jump':pyglet.window.key.W,
            'reset':pyglet.window.key.R,
            'm1': 1,
            'm2': 4,
            'pause': pyglet.window.key.ENTER
        }

        self.quit = False #not sure if I need this...

        self.gameStates = {
            'main menu': 0,
            'map select': 1,
            'play': 2,
            'result': 3,
            'pause': 4,
            'settings': 5,
            'quit': 6
        }

        self.currState = 2  #for now
        self.collisionOffset = 0

        self.maps = []
        self.currentMap = None
        self.currentLevel = None
        self.mapIndex = 0
        self.levelIndex = 0

        #Sprite batches divided into three groups: foreground, ground and background.
        #These are not in reference to classes, but to which layer something is in.
        self.drawBatch = pyglet.graphics.Batch()
        self.renderGroup = pyglet.graphics.Group()
        self.skyGroup = pyglet.graphics.OrderedGroup(0, self.renderGroup)
        self.treeGroup1 = pyglet.graphics.OrderedGroup(1, self.renderGroup)
        self.treeGroup2 = pyglet.graphics.OrderedGroup(2, self.renderGroup)
        self.bgGroup = pyglet.graphics.OrderedGroup(3, self.renderGroup)  #group for bg items
        self.gGroup = pyglet.graphics.OrderedGroup(4, self.renderGroup)   #group for ground items (ground, utilities, projectiles, explosionsé...)
        self.eGroup = pyglet.graphics.OrderedGroup(5, self.renderGroup)   #group for explosions
        self.pGroup = pyglet.graphics.OrderedGroup(6, self.renderGroup)   #group for player
        self.fgGroup = pyglet.graphics.OrderedGroup(7, self.renderGroup)  #group for foreground items

        self.cursor = self.get_system_mouse_cursor(self.CURSOR_CROSSHAIR)
        self.set_mouse_cursor(self.cursor)
        #self.set_fullscreen(True)

        self.camera = Camera(0,0,self.gameWidth, self.gameHeight)
        self.timer = 0
        self.rocketCount = 0

        self.fontSize = 16
        self.font = 'Consolas'
        self.rocketLabelText = "Jumps:"
        self.timeLabelText = "Time:"

        self.timeText = pyglet.text.Label("Time:",
                  font_name=self.font,
                  font_size=self.fontSize,
                  x=60, y=30,
                  anchor_x='center', anchor_y='center')

        self.timeLabel = pyglet.text.Label(str(self.timer),
                  font_name=self.font,
                  font_size=self.fontSize,
                  x=self.timeText.x + self.timeText.font_size *len(self.timeLabelText), y=self.timeText.y,
                  anchor_x='center', anchor_y='center')

        self.rocketText = pyglet.text.Label(self.rocketLabelText,
                  font_name=self.font,
                  font_size=self.fontSize,
                  x=self.gameWidth - len(self.rocketLabelText)*self.fontSize, y=30,
                  anchor_x='center', anchor_y='center')

        self.rocketCountLabel = pyglet.text.Label(str(self.rocketCount),
                  font_name=self.font,
                  font_size=self.fontSize,
                  x=self.gameWidth - len(str(self.rocketCount))*self.fontSize -10, y=30,
                  anchor_x='center', anchor_y='center')


    def setup(self):
       #setup first map only...setup the next ones once the player completes
        self.maps.append(basicMap.build(self.tileSize))

        self.mapIndex = 0
        self.levelIndex = 0

        self.currentMap = self.maps[self.mapIndex]

        self.loadNextLevel()

        self.clock.schedule_interval(self.update, 1 / self.clockRate)

    def loadNextLevel(self):
        if self.currentLevel is not None:
            self.clearLevel()
            self.levelIndex += 1
            
        self.currentLevel = self.currentMap.levels[self.levelIndex]
        self.camera.xPos = self.currentLevel.camPos[0]
        self.camera.yPos = self.currentLevel.camPos[1]

    def clearLevel(self):     
        toClear = [
            self.currentLevel.foreground,
            self.currentLevel.ground,
            self.currentLevel.utils,
            self.currentLevel.background,
            self.currentLevel.explosions
        ]

        for tc in toClear:
            for e in tc:
                if e.sprite is not None:
                    e.sprite.batch = None
                    e.sprite.group = None

                if isinstance(e, GroundGrid):
                    for g in e.ground:
                        if e.sprite is not None:
                            g.sprite.batch = None
                            g.sprite.group = None

        self.currentLevel.player.sprite.batch = None
        self.currentLevel.player.sprite.group = None

        
        self.timer = 0
        self.rocketCount = 0
        self.rocketCountLabel.text = str(self.rocketCount)
        GlobalTick.value = 0
    

    def update(self,dt): #game loop
        if self.currState == self.gameStates.get('play'):
            toDel = []

            player = self.currentLevel.player
            level = self.currentLevel

            player.update(self.camera, self.drawBatch, self.gGroup, player)
            self.checkPreviousCollisions(player)
            self.checkCollisions(player, level.tileMap)
            self.checkNextStepCollisions(player, level.tileMap)
            level.updateTileMap(player)

            if player.atJumpShootingFrame:
                if player.facingLeft:
                    x = 225
                else:
                    x = -110

                y = -45

                e = Explosion(player.xPos,player.yPos, GlobalTick.value, player.facingLeft, x, y)
                level.addExplosionToLevel(e)

            scroll = self.checkScroll(player)

            scrolled = scroll[0]
            horizontal = scroll[1]
            vertical = scroll[2]

            if scrolled:
                xSpeed = 0
                ySpeed = 0

                if horizontal:
                    xSpeed = player.xSpeed
                if vertical:
                    ySpeed = player.ySpeed

                self.camera.move(xSpeed, ySpeed)

            #for p in self.currentLevel.projectiles:
              #  p.update(self.camera, self.drawBatch, self.pGroup, player)
               # self.checkCollisions(p, level.tileMap)
               # level.updateTileMap(p)

                #if p.isExploding:
                   # e = Explosion(p.xPos + p.width/2, p.yPos + p.height/2, GlobalTick.value)
                    #level.addExplosionToLevel(e)
                    #toDel.append(p)

            for u in self.currentLevel.utils:
                u.update(self.camera, self.drawBatch, self.bgGroup, player)
                u.checkPlayerPos(player)

                if u.isHit:
                    self.loadNextLevel()

            for e in self.currentLevel.explosions:
                e.update(self.camera, self.drawBatch, self.eGroup, player)

                if e.isFlaggedForDel:
                    toDel.append(e)

            for g in self.currentLevel.ground:
                g.update(self.camera, self.drawBatch, self.gGroup, player)

            for bg in self.currentLevel.background:
                group = self.bgGroup

                if isinstance(bg, Cloud1) or isinstance(bg, Cloud2):
                    group = self.skyGroup
                elif isinstance(bg, TreeBack1) or isinstance(bg, TreeBack2) or isinstance(bg, TreeBack3):
                    group = self.treeGroup1
                elif isinstance(bg, TreeFront1) or isinstance(bg, TreeFront2) or isinstance(bg, TreeFront3):
                    group = self.treeGroup2
                elif isinstance(bg, Sun):
                    group = self.skyGroup

                bg.update(self.camera, self.drawBatch, group, player)

            for td in toDel:
                self.removeEntity(td)

            self.timer = int(GlobalTick.value/self.clockRate)
            self.timeLabel.text  = str(self.timer)

            if self.timer > 999:
                self.timeLabel.x += self.fontSize

            GlobalTick.value += 1

        elif self.currState == self.gameStates.get('pause'):
            pass

    def drawSky(self):

        if self.levelIndex == 0:
        
            pyglet.graphics.draw_indexed(4, pyglet.gl.GL_TRIANGLES,  #background color yay openGL!
                [0, 1, 2, 0, 2, 3],
                ('v2i', (0, 0,
                         self.width, 0,
                         self.width, self.height,
                         0, self.height)),
                ('c3B', (208,130,83,
                         208,130,83,
                         255,255,255,
                         208,130,83))       #208,130,83
            )
            
        if self.levelIndex == 1:
            pyglet.graphics.draw_indexed(4, pyglet.gl.GL_TRIANGLES,  #background color yay openGL!
                [0, 1, 2, 0, 2, 3],
                ('v2i', (0, 0,
                         self.width, 0,
                         self.width, self.height,
                         0, self.height)),
                ('c3B', (255,0,0,
                         0,255,0,
                         0,0,255,
                         255,255,255))
            )

    def on_draw(self):
        self.clear()
        self.drawSky()

        self.drawBatch.draw()
        self.timeLabel.draw()
        self.timeText.draw()
        self.rocketText.draw()
        self.rocketCountLabel.draw()

        if self.currState == self.gameStates.get('pause'):
            self.drawPauseOverlay()

    def drawPauseOverlay(self):
        pyglet.gl.glEnable (GL_BLEND);
        pyglet.gl.glBlendFunc (GL_ONE, GL_ONE);
        pyglet.graphics.draw_indexed(4, pyglet.gl.GL_TRIANGLES,  #background color yay openGL!
            [0, 1, 2, 0, 2, 3],
            ('v2i', (0, 0,
                     self.width, 0,
                     self.width, self.height,
                     0, self.height)),
            ('c3B', (50,50,50,
                     50,50,50,
                     50,50,50,
                     50,50,50))
        )

    def setupBatch(self, entities, batch):
        for e in entities:
            e.sprite.batch = batch

    def on_key_press(self, symbol, modifiers):
        if self.currState == self.gameStates.get('play'):
            if symbol == self.controls.get('left'):
                self.currentLevel.player.isWalking = -1
            elif symbol == self.controls.get('right'):
                self.currentLevel.player.isWalking = 1
            elif symbol == self.controls.get('jump'):
                if self.currentLevel.player.jump():
                    self.rocketCount += 1
                    self.rocketCountLabel.text = str(self.rocketCount)
            elif symbol == self.controls.get('reset'):
                print("press reset")

        if symbol == self.controls.get('pause'):
            if self.currState == self.gameStates.get('play'):
                self.currState = self.pause()
            else:
                self.currState = self.unpause()

    def on_key_release(self, symbol, modifiers):
        if self.currState == self.gameStates.get('play'):
            if symbol == self.controls.get('left') or symbol == self.controls.get('right'):
                self.currentLevel.player.resetWalk()
           # elif symbol == self.controls.get('jump'):
                #print("release jump")
            elif symbol == self.controls.get('reset'):
                print("release reset")

    def on_mouse_press(self,x,y,button,modifiers):
        if button == 1:
            if self.currState == self.gameStates.get('play'):
                proj = None #self.currentLevel.player.shoot(x,y, self.camera, True)
                if proj is not None:
                    self.currentLevel.addProjectileToLevel(proj)
                    self.rocketCount += 1
                    self.rocketCountLabel.text = str(self.rocketCount)

    def checkScroll(self, player):     #check if needs to be moved which causes a scroll
        scroll = False
        horizontal = False
        vertical = False

        if player.xSpeed != 0:
            if player.xSpeed < 0:
                if player.xPos < self.camera.xPos + self.camera.checkXOffset:
                    scroll = True

            elif  player.xSpeed > 0:
                if player.xPos + player.width > (self.camera.xPos + self.camera.width - self.camera.checkXOffset):
                    scroll = True

            horizontal = True


        """if player.ySpeed != 0:
            if player.ySpeed < 0:
                if player.yPos < self.camera.yPos + self.camera.checkYOffset:
                    scroll = True

            elif player.ySpeed > 0:
                if player.yPos + player.height > (self.camera.yPos + self.camera.height - self.camera.checkYOffset):
                    scroll = True

            vertical = True"""

        return (scroll, horizontal, vertical)

    def checkCollisions(self,entity, tileMap):
        #For every entity you need to check
        #check if it actually is ground
        #check if there is collision between both AABBs
        #make sure the collision doesnt already exist
        #get collision angle

        entitiesToCheck = self.getEntitiesToCheck(entity,tileMap)
        collisions = []

        for e in entitiesToCheck:
            if self.checkCollAABBvsAABB(entity.aabb, e.aabb):
                if not self.checkCollAlreadyExists(entity.collisions, e):
                    angleFrom = self.getCollisionAngle(entity, e)
                    angleTo = self.getCollisionAngle(e, entity)

                    middleE1 = (entity.xPos + entity.width/2, entity.yPos + entity.height/2)
                    middleE2 = (e.xPos + e.width/2, e.yPos + e.height/2)
                    
                    distSQ = self.getDistanceSQ(middleE1, middleE2)

                    dir = self.getCollisionDir(entity.aabb, e.aabb)

                    collision = Collision(e,dir,angleFrom, angleTo, distSQ)
                    collisions.append(collision)
                    entity.collisions.append(collision)

        return collisions

    def checkNextStepCollisions(self, entity, tileMap): #please complete this!
        if entity.ySpeed != 0 and entity.xSpeed != 0:    
            nextStepE = copy.copy(entity) #on veut une copie, pas une reference!!
            
            nextStepE.xPos += nextStepE.xSpeed
            nextStepE.yPos += nextStepE.ySpeed

            #collisions = self.checkCollisions(nextStepE, tileMap)

            #for c in collisions:
                #pass

    def getCollisionDir(self, AABB1, AABB2):
        #horiz = "no"
        #verti = "no"

        dir = ""

        m1x = AABB1["x"] + AABB1["width"]/2  #middle points of each AABB to get distance
        m1y = AABB1["y"] + AABB1["height"]/2

        m2x = AABB2["x"] + AABB2["width"]/2
        m2y = AABB2["y"] + AABB2["height"]/2

        hd = math.fabs(m1x - m2x)
        vd = math.fabs(m1y - m2y)

        #incoming epic ifs! oh boy!

        if AABB1["x"] < AABB2["x"] + AABB2["width"] and AABB1["x"] + AABB1["width"] > AABB2["x"] + AABB2["width"] and AABB2["y"] < m1y < AABB2["y"] + AABB2["height"]:
            if m1x > m2x:
                dir = "left"

        elif AABB1["x"] + AABB1["width"] > AABB2["x"] and AABB1["x"] + AABB1["width"] > AABB2["x"] and AABB2["y"] < m1y < AABB2["y"] + AABB2["height"]:
            if m1x < m2x:
                dir = "right"


        if AABB1["y"] < AABB2["y"] + AABB2["height"] and AABB1["y"] + AABB1["height"] > AABB2["y"] + AABB2["height"] and AABB2["x"] < m1x < AABB2["x"] + AABB2["width"]:
            if m1y > m2y:
                dir = "down"

        elif AABB1["y"] + AABB1["height"] > AABB2["y"] and AABB1["y"] < AABB2["y"] and AABB2["x"] < m1x < AABB2["x"] + AABB2["width"]:
            if m1y < m2y:
                dir = "up"

        """ if hd < vd:
            if m1x < m2x:

                dir = "right"
            else:
                dir = "left"
        elif vd < hd:
            if m1y < m2y:
                dir = "up"
            else:
                dir = "down"""""
            
        return dir

    def checkPreviousCollisions(self,entity):
        toDel = []

        for i in reversed(range(len(entity.collisions))):
            if not self.checkCollAABBvsAABB(entity.aabb, entity.collisions[i].entity.aabb):
                toDel.append(i)

        for i in toDel:
            del entity.collisions[i]

    def checkCollAABBvsAABB(self, AABB1, AABB2):
        #rect1.x <= rect2.x + rect2.width &&
        # rect1.x + rect1.width >= rect2.x &&
        # rect1.y <= rect2.y + rect2.height &&
        # rect1.height + rect1.y >=rect2.y

        collision = False

        if AABB1["x"] <= AABB2["x"] + AABB2["width"] and AABB1["x"] + AABB1["width"] >= AABB2["x"] and AABB1["y"] <=  AABB2["y"] + AABB2["height"] and AABB1["y"] +  AABB1["height"] >=  AABB2["y"]:
            collision =  True
        else:
            collision =  False

        return collision

    def checkCollAlreadyExists(self, entityCollisions, entityToCheck):
        exists = False

        if entityToCheck in entityCollisions:
            exists = True

        return exists

    def getCollisionAngle(self, entity1, entity2):
        e1MiddleCoords = (entity1.xPos + entity1.width/2,entity1.yPos + entity1.height/2)
        e2MiddleCoords = (entity2.xPos + entity2.width/2,entity2.yPos + entity2.height/2)

        dx = e2MiddleCoords[0] - e1MiddleCoords[0]
        dy = e2MiddleCoords[1] - e1MiddleCoords[1]

        degs = math.degrees(math.atan2(dy, dx))

        if degs < 0:
            degs = 360 + degs

        return degs

    def getDistanceSQ(self, point1, point2):
        x1 = point1[0]
        x2 = point2[0]

        y1 = point1[1]
        y2 = point2[1]

        dx = (x2-x1) * (x2-x1)
        dy = (y2-y1) * (y2-y1)

        return dx + dy

    def getEntitiesToCheck(self, entity, tileMap):
        coords = []
        entitiesToCheck = []

        #get bordering tiles
        for t in entity.occupiedTiles:
            x = t.xTile
            y = t.yTile

            for i in range(-1,2,1):
                for j in range(-1,2,1):
                    coords.append((x + i,y + j))

        for c in coords:
            x = c[0]
            y = c[1]

            tile = tileMap[x][y]

            if tile.isOccupied:
                for e in tile.entities:
                    if e.isCollidable:
                        if e != entity:
                            if e not in entitiesToCheck:
                                entitiesToCheck.append(e)

        return entitiesToCheck

    def removeFromView(self, entity):
        entity.sprite.delete()

    def removeEntity(self, entity):
        if entity.sprite:
            entity.sprite.batch = None
            entity.sprite.group = None
            entity.sprite.delete()

        if isinstance(entity, Projectile):
            self.currentLevel.projectiles.remove(entity)
        elif isinstance(entity, Explosion):
            self.currentLevel.explosions.remove(entity)

        self.currentLevel.removeEntityFromTileMap(entity)

    def checkMapCompletion(self, currMap):
        pass

    def checkLevelCompletion(self, currLevel):
        pass

    def play(self):
        return 1

    def pause(self):
        return 4

    def unpause(self):
        return 2

    def quitMap(self):
        return 1

    def resetLevel(self):
        pass
        #call level reset
        #player pos = spawn pos

    def changeSettings(self):
        pass
        #return dictionary

    def showMainMenu(self):
        return 0

    def showMapSelect(self, maps):
        return 1

    def showLevel(self, level):
        return 2

    def showResult(self, map):
        return 3

    def showPause(self):
        return 4

    def showSettings(self):
        return 5

if __name__ == '__main__':
    game = Game()
    game.setup()
    pyglet.app.run()
