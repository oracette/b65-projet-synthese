import math

class Force:
    def __init__(self,force,angle):
        self.force = force
        self.angle = angle #need angle from explosion to robot, currently have robot to explosion
        self.done = False
        self.add = 5

    def apply(self, target):
        #Applies the force to whatever parent has it.
        #angle needs to be in RADS

        xSpeed =  self.force * math.cos(self.angle) + self.add
        ySpeed = self.force * math.sin(self.angle) + self.add

        #if math.pi/2 < self.angle < 3*math.pi/2:
         #   xSpeed = -xSpeed

        print(xSpeed)

        #target.xSpeed = xSpeed
        target.ySpeed = ySpeed

        target.isRJing = True

        degs = math.degrees(self.angle)
        
        self.force -= 1

        if self.force <= 0:
            self.done = True