import math
from Tile import *
from Entities import Ground

class Level:
    def __init__(self,width, height, foreground, ground, player, utils, background, camPos):
        self.width = width
        self.height = height
        self.player = None
        self.foreground = foreground
        self.ground = ground
        self.player = player
        self.utils = utils
        self.background = background
        self.projectiles = []
        self.explosions = []
        self.hasScrolled = False
        self.scrollDir = None
        self.camPos = camPos

        #arbitrary value for now, please fix
        self.spawnTile = (0,0)

        #Tiles are square & factor of 2!
        #hard coded tileSize...
        self.tileSize = 32

        #The map builds itself
        self.tmHeight = 0
        self.tmWidth = 0
        self.tileMap = self.setupTileMap()

        self.build()

        #print(self.tmHeight, self.tmWidth)

    def reset(self):
        pass

    def print2DArrayToFile(self, array): #for debugging
        string = ""

        file = open("bitmap.txt", "w")

        for row in array:
            for val in row:
                string += "[" + str(val) + "]" + " "
            string += "\n"
            file.write(string)
            string = ""


    def setupTileMap(self):
        #Convert width & height to tiles
        nbTileWidth = int(self.width/self.tileSize)
        nbTileHeight = int(self.height/self.tileSize)
        #set = [[Tile(x*self.tileSize, y*self.tileSize, self.tileSize) for y in range(nbTileHeight)] for x in range(nbTileWidth)]

        set = []

        for x in range(nbTileWidth):
            row = []
            for y in range(nbTileHeight):
                tile = Tile(x*self.tileSize, y*self.tileSize, self.tileSize)
                row.append(tile)
            set.append(row)

        self.tmHeight = nbTileHeight
        self.tmWidth = nbTileWidth

        return set

    def assignTile(self, entity, xTile, yTile, width, height):
        #Link the tile with the entity & floating number management
        #if pos is float, round it down (math.floor) and add 1 to width or height, depending on what is float.
        #decided not to use isinstance, since 1.0 is considered a float, and so width & height would still be affected even if the decimal is 0.

        if not xTile.is_integer():
            xTile = math.floor(xTile) #already supposed to return int, but returns me a number with decimal 0...floor & add 1 instead of ceil
            width += 1

        if not yTile.is_integer():
            yTile = math.floor(yTile)
            height += 1

        xTile = int(xTile)
        yTile = int(yTile)

        for x in range(width):
            for y in range(height):
                tile = self.tileMap[xTile + x][yTile + y]
                tile.entities.append(entity)
                tile.isOccupied = True
                entity.occupiedTiles.append(tile)

                if isinstance(entity, Ground):
                    tile.isGround = True

    def build(self):
        #for all entities, place them in their respective tiles
        #foreground, ground, player, utils, background

        for fg in self.foreground:
            self.setupEntityToTile(fg)

        for g in self.ground:
            self.setupEntityToTile(g)

        self.setupEntityToTile(self.player)

        for u in self.utils:
            self.setupEntityToTile(u)

        #for bg in self.background: #Background elements arent actually part of the game matrix...they just appear, they don't act, for now
         #   self.setupEntityToTile(bg)

    def updateTileMap(self, entity):
        #On recoit entity qui a change de position.
        #Seuls les xPos et yPos ont ete change a date. Pas occupiedTiles.
        #va ou il etait, enleve le et remet le a sa nouvelle position.
        #AJOUT: check s'il y existait un autre entity sur cette meme case. Si oui, remet la.

        #for t in entity.occupiedTiles:
           # self.removeEntityFromTile(entity, t)

        self.removeEntityFromTileMap(entity)

        entity.occupiedTiles = []
        self.setupEntityToTile(entity)

    def setupEntityToTile(self, entity):
        coords = self.pixelToTileCoords(entity.xPos, entity.yPos)

        xTile = coords[0]
        yTile = coords[1]

        width = math.ceil(entity.width / self.tileSize)
        height = math.ceil(entity.height / self.tileSize)

        self.assignTile(entity, xTile, yTile, width, height)

    def removeEntityFromTile(self, entity, tile):
        if entity in tile.entities:
            tile.entities.remove(entity)

        if not tile.entities:
            tile.isOccupied = False

        return tile

    def pixelToTileCoords(self, xPixelPos, yPixelPos):
        xTile = xPixelPos / self.tileSize
        yTile = yPixelPos / self.tileSize

        return (xTile, yTile)

    def addProjectileToLevel(self, entity):
        self.projectiles.append(entity)
        self.setupEntityToTile(entity)

    def addExplosionToLevel(self, entity):
        self.explosions.append(entity)
        self.setupEntityToTile(entity)

    def removeEntityFromTileMap(self, entity):
        for t in entity.occupiedTiles:
            self.removeEntityFromTile(entity,t)

    def scroll(self, xSpeed, ySpeed):
        #Dont need to update the tilemap since nothing actually moved!

        toUpdate = [
            self.foreground,
            self.ground,
            self.utils,
            self.background,
            self.explosions
        ]

        for update in toUpdate:
            for e in update:
                #Don't affect game pos, affect cam pos
                e.xCam += xSpeed
                e.yCam += ySpeed

        self.player.xCam += xSpeed
        self.player.yCam += ySpeed
