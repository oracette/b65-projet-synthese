class Camera:
    def __init__(self, x, y, width, height):
        self.xPos = x
        self.yPos = y
        self.width = width
        self.height = height
        self.checkXOffset = 128
        self.checkYOffset = 200

    def move(self, x, y):
        self.xPos += x
        self.yPos += y

    def moveTo(self, x, y):
        self.xPos = x
        self.yPos = y