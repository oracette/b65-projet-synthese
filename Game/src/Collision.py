class Collision:
    def __init__(self, entity, dir,angleFrom, angleTo, distSQ):
        #dir can be up,down, left, right
        self.angleFrom = angleFrom
        self.angleTo = angleTo
        self.entity = entity
        self.distSQ = distSQ
        self.dir = dir