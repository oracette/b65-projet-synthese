from pyglet.sprite import Sprite
from pyglet.image import *
from Collision import *
from Force import *

import GlobalTick
import GlobalId
import math
import random

#cats animations are 24 fps

###########Game Entity super class##########
class GameEntity:
    def __init__(self, x, y, width, height):
        self.xPos = x
        self.yPos = y
        self.width = width
        self.height = height
        self.xOffset = 0
        self.yOffset = 0
        self.xCam = None
        self.yCam = None
        self.image = None
        self.sprite = None
        self.isCollidable = False
        self.isUpdatable = False
        self.isDrawn = False
        self.occupiedTiles = []
        self.isHittable = False
        self.isHit = False
        self.animBegin = 0
        self.animFrame = 0
        self.animEnd = 0
        self.animTime = 0
        self.collisions = []
        self.groundCollide = False
        self.isFlaggedForDel = False
        self.inCam = False
        self.scrolled = False
        self.scale = 1
        self.aabb = {
            "x": self.xPos,
            "y": self.yPos,
            "width": self.width,
            "height": self.height
        }
        self.animFPS = 1/24

    def update(self, camera, batch, group, player):
        inCam = self.isInCamera(camera)

        if inCam:
            self.getRelCamCoords(camera.xPos, camera.yPos)
            if self.sprite is None:
                self.createSprite()
            else:
                self.updateSprite()
            self.sprite.batch = batch
            self.sprite.group = group

        elif not inCam and self.sprite is not None:
            self.sprite.batch = None
            self.sprite.group = None

        if self.isHittable:
            self.checkPlayerPos(player)

            if self.isHit:
                self.onHit()

        self.customUpdate()

        if self.isCollidable:
            self.updateAABB()

    def collide(self):
        pass

    def createAnims(self):
        pass

    def isInCamera(self, camera):

        isInCam = True

        if self.xPos + self.width < camera.xPos:
            isInCam = False
        if self.xPos > camera.xPos + camera.width :
            isInCam = False
        if self.yPos < camera.yPos:
            isInCam = False
        if self.yPos + self.height > camera.yPos + camera.height:
            isInCam = False

        return isInCam

    def updateSprite(self):
        self.sprite.x = self.xCam + self.xOffset
        self.sprite.y = self.yCam + self.yOffset
        self.sprite.scale = self.scale

    def getAngle(self, point1, point2):
        #Taken from StackOverflow!!!
        dx = point2[0] - point1[0]
        dy = point2[1] - point1[1]

        rads = math.atan2(dy, dx)
        rads %= 2*math.pi
        degs = math.degrees(rads)

        return degs

    def updateAABB(self):
        self.aabb = {
            "x": self.xPos,
            "y": self.yPos,
            "width": self.width,
            "height": self.height
        }

    def scroll(self, x, y): #WHAT IS A SCROLL? It's when you move only on the screen, not in the model
        self.xCam += x
        self.yCam += y

    def teleport(self, x, y):
        self.xPos = x
        self.yPos = y

    def onHit(self):
        pass

    def getRelCamCoords(self, camX, camY):
        self.xCam = self.xPos - camX
        self.yCam = self.yPos - camY

    def createSprite(self):
        self.sprite = Sprite(self.image, self.xCam, self.yCam) # subpixel='rue' for floating point sprite

    def customUpdate(self):
        pass

    def checkGroundCollision(self):
        coll = False

        for c in self.collisions:
            if c.entity.yPos + c.entity.height >= self.yPos:
                if c.entity.xPos < self.xPos:
                    if c.entity.xPos + c.entity.width >= self.xPos and c.entity.xPos + c.entity.width <= self.xPos + self.width:
                        coll = True
                elif c.entity.xPos > self.xPos:
                    if c.entity.xPos > self.xPos and c.entity.xPos <= self.xPos + self.width:
                        coll = True

        return coll

    def getClosestGround(self):
        ys = []
        
        for c in self.collisions:
            if c.dir[1] == "down":
                ys.append(c.entity.yPos + c.entity.height)

        return max(ys)

    def getNewX(self, left):
        xs = []
        
        if left:
            dir = "left"
        else:
            dir = "right"

        for c in self.collisions:
            if c.dir[0] == dir:
                if left:
                    x = c.entity.xPos 
                else:
                    x = c.entity.xPos + c.entity.width
                    
                xs.append(x)

        return self.xPos

    def animate(self):
        pass

    def checkPlayerPos(self, player):
        pass

###########Moving Entities##########

class MovingEntity(GameEntity):
    def __init__(self,x, y, width, height):
        GameEntity.__init__(self,x, y, width, height)
        self.xSpeed = 0
        self.xVelocity = 0

        self.ySpeed = 0
        self.yVelocity = 0

        self.isMovingLeft = False
        self.isMovingRight = False
        self.isMovingUp = False
        self.isMovingDown = False
        self.isFixed = False

    def customUpdate(self):
        #X axis
        self.xSpeed += self.xVelocity
        self.xPos += self.xSpeed

        #Y axis
        self.ySpeed += self.yVelocity
        self.yPos += self.ySpeed

        return True

    def handleCollisions(self):
        pass

###########Robot##########
class Robot(MovingEntity):
    def __init__(self,x, y):
        MovingEntity.__init__(self,x, y, 92,32)
        self.hasMoved = False
        self.gun = None
        self.forces = []
        self.state = 0
        self.possibleStates = {
            'fixed': 0,
            'walking_left': 1,
            'walking_right': 2,
            'jumping': 3,
            'falling': 4
        }
        self.isCollidable = True
        self.isWalking = 0
        self.isFalling = True

        self.jumpTimer = None
        self.jumpTime = 8
        self.jumpStart = None
        self.jumpEnd = None
        self.isJumping = False
        self.jumpSpeed = 1
        self.iniJumpSpeed = 1
        self.jumpVelocity = 0.6
        self.startJump = False

        self.pSpeed = 6
        self.pVelocity = 0.6
        self.initPSpeed = 6

        self.rjTimer = None
        self.rjTime = None
        self.rjStart = None
        self.rjEnd =None
        self.isRJing = False

        self.walkTimer = 0
        self.shootTimer = 0

        self.isUpdatable = True

        self.image = load("../res/img/droid/droid_min_sheet.png") #original image is 307 * 109...scaled down to 0.3 its 92 * 32
        #self.image = load("../res/img/entities/testing/robot.png")

        self.sheet = ImageGrid(self.image, 1,5,414,106,0,0) #image, rows, columns, item_width=None, item_height=None, row_padding=0, column_padding=0 robot sprite sheet img size: 414 * 106
        self.texture = TextureGrid(self.sheet)

        self.leftTexture = self.texture.get_transform(False,True,0)

        self.animState = 0
        self.jumpAnim = []
        self.jumpAnimCount = 0
        self.jumpAnimMax = 0

        self.walkSpeed = 0.5
        self.initWalkSpeed = 0.2
        self.walkVelocity = 0.5
        self.maxWalkSpeed = 2

        self.fallSpeed = 13
        self.fallVelocity = 0.8
        self.initFallSpeed = 13
        self.maxFallSpeed = 40

        self.xShootOffset = self.width + 10
        self.yShootOffset = self.height/2

        self.shootDelay = 0
        self.lastShot = 0

        self.forces = []
        self.scale = 0.4

        self.xCollideSlowDown = 3
        self.xSpeedResolution = 1
        self.createAnims()

        self.nbBounce = 0
        self.bounceSlowDown = 3
        self.maxBounce = 3

        self.facingLeft = False

        self.yOffset = -2

        self.atJumpShootingFrame = False

        self.aabb = {
            "x": self.xPos,
            "y": self.yPos,
            "width": self.width,
            "height": self.height
        }


    def customUpdate(self):
        dirs = self.getCollidingDir()  #returns an array of 4 bool for left, right, up, down
        self.handleCollisions(dirs[0], dirs[1])

        if self.isWalking != 0:
            self.walk(self.isWalking)

        if self.isFalling:
            self.fall()

        if self.isJumping:
            self.jumpTick()
            if self.jumpTimer == self.jumpEnd:
                self.resetJump()
                self.fall()

        if self.facingLeft:
            self.xOffset = 360 * self.scale
        else:
            self.xOffset = -30

        self.yPos += self.ySpeed
        self.xPos += self.xSpeed

        self.animate()

    def createAnims(self):
        #normally we need to setup the image sequence first...since the sprite sheet contains the only anim, import all from the sprite sheet.

        for frame in self.texture:
            self.jumpAnim.append(frame)
            self.jumpAnimMax += 1

    def animate(self):
        if self.isJumping and self.jumpAnimCount < len(self.texture):
            img = self.texture.get(0,self.jumpAnimCount)
            self.jumpAnimCount += 1
        else:
            img = self.texture.get(0,0)
            self.jumpAnimCount = 0

        if self.jumpAnimCount == len(self.texture) - 1:
            self.atJumpShootingFrame = True
        else:
            self.atJumpShootingFrame = False

        img = img.get_transform(self.facingLeft, False, 0)

        self.sprite.image = img

        self.updateSprite()

    def fall(self):
        self.ySpeed -= self.fallVelocity
        self.isFalling = True

    def jumpTick(self):
        #if self.jumpTimer <= self.jumpEnd:
        self.xSpeed = 0
        self.jumpTimer += 1
        self.jumpSpeed += self.jumpVelocity
        self.ySpeed += self.jumpSpeed

        self.pSpeed += self.pVelocity

        if self.facingLeft:
            self.xSpeed -= self.pSpeed
        else:
            self.xSpeed += self.pSpeed

    def jump(self):
        jump = False
        
        if self.jumpTimer is None:  #a none jumpTimer means he was not already jumping when the key was pressed
            if not self.isFalling:   #need to make sure hes also not already falling
                self.jumpTimer = GlobalTick.value
                self.jumpStart = GlobalTick.value
                self.jumpEnd = self.jumpStart + self.jumpTime
                self.isJumping = True

                jump = True
                
        return jump

    def walk(self, dir):
        if not self.isJumping and self.nbBounce == 0: #and not self.isFalling:
            if self.walkSpeed < self.maxWalkSpeed:
                self.walkSpeed += self.walkVelocity
                
                if dir == -1:
                    self.xSpeed -= self.walkSpeed
                elif dir == 1:
                    self.xSpeed += self.walkSpeed

        if dir == -1:
            self.facingLeft = True
        elif dir == 1:
            self.facingLeft = False

    def handleCollisions(self, dirs, newPos): #left = 0, right = 1, up = 2 down = 3
        
        if newPos[1] is not None:
            self.yPos = newPos[1]

        if dirs[0] or dirs[1]:
            if self.ySpeed != 0:
                self.fall()

        if dirs[0]:
            self.xSpeed = math.fabs(self.xSpeed) / self.xCollideSlowDown
            self.isWalking = 0
            if newPos[0] is not None:
                self.xPos = newPos[0]
        
        if dirs[1]:
            self.xSpeed = -self.xSpeed / self.xCollideSlowDown
            self.isWalking = 0
            if newPos[0] is not None:
                self.xPos = newPos[0] - 130 #lol

        if dirs[2]:
            self.ySpeed = 0
            self.fall()

        if dirs[3]:
            self.isFalling = False
            self.ySpeed = -self.ySpeed / self.bounceSlowDown
            self.nbBounce += 1

            if self.nbBounce > 0 and self.ySpeed == 0:
                self.nbBounce = 0

            if self.nbBounce > 0:
                self.xSpeed = self.xSpeed / self.xCollideSlowDown

            if self.nbBounce == self.maxBounce:
                self.ySpeed = 0
                self.xSpeed = 0
                self.nbBounce = 0
        else:
            self.isFalling = True

    def getCollidingDir(self):
        dcoll = False
        ucoll = False
        lcoll = False
        rcoll = False

        newX = None
        newY = None

        xOffset = 1

        for c in self.collisions:
            cx = c.entity.xPos
            cy = c.entity.yPos
            cw = c.entity.width
            ch = c.entity.height

            if c.dir == "left":
                lcoll = True
                newX = cx + cw

            elif c.dir == "right":
                rcoll = True
                newX = cx

            if c.dir == "up":
                ucoll = True
                newY = None

            elif c.dir == "down":
                dcoll = True
                if newY is None:
                    newY = cy + ch
                elif cy + ch  < newY:
                    newY = cy + ch

        return ([lcoll, rcoll, ucoll, dcoll],(newX, newY))  #bon j'avoue c'est pas tres beau...j'en ai besoin!!

    def handleForces(self):
        toDel = []

        for f in self.forces:
            f.apply(self)
            if f.done:
                toDel.append(f)

        for td in toDel:
            self.forces.remove(f)

        if not self.forces:
            self.isRJing = False

        del toDel

    def resetWalk(self):
        #if not self.isJumping and not self.isFalling:
        self.xSpeed = 0
        self.walkSpeed = self.initWalkSpeed
        self.isWalking = 0

    def shoot(self,xClick, yClick, camera, isRocket):
        p = None

        if GlobalTick.value - self.lastShot >= self.shootDelay:
            self.lastShot = GlobalTick.value

            #Originate from middle. Add an offset if needed later...
            xOrigin = self.xPos + self.width/2
            yOrigin = self.yPos + self.height/2 - 10

            clickXOffset = xClick + camera.xPos
            clickYOffset = yClick + camera.yPos

            angle = self.calcShootAngle(xOrigin,yOrigin,clickXOffset,clickYOffset)

            #create projectile and return it to append to level entitites
            if isRocket:
                p = Rocket(xOrigin,yOrigin,angle)

            GlobalId.value += 1

        return p

    def calcShootAngle(self,x1,y1,x2,y2):
        dx = x2 - x1
        dy = y2 - y1

        return math.atan2(dy,dx)

    def resetJump(self):
        self.jumpTimer = None
        self.jumpStart = None
        self.jumpEnd = None
        self.isJumping = False
        self.isMovingUp = False
        self.jumpSpeed = self.iniJumpSpeed
        self.pSpeed = self.initPSpeed

    def resetFall(self):
        self.isFalling = False

##########Projectile##########

#Useless class right for now :(
class Projectile(MovingEntity):
    def __init__(self,x, y, angle, width, height):
        MovingEntity.__init__(self,x, y, width, height)
        self.image = load("../res/img/entities/testing/projectile.png")
        self.sprite = Sprite(self.image, self.xPos, self.yPos)
        self.angle = angle
        self.createTIme = GlobalTick.value
        self.collideEffect = None #WIll be an explosion
        self.isExploding = False
        self.isCollidable = True


    def customUpdate(self):
        self.handleCollisions()

        if not self.isExploding:
            #calculate new point of projectile
            if self.xSpeed <= self.maxXSpeed:
                self.xSpeed += self.xVelocity

            if self.ySpeed <= self.maxYSpeed:
                self.ySpeed += self.yVelocity

            #needs to be redone since I figured out how atan2 works

            self.xPos = (math.cos(self.angle)*self.xSpeed)+self.xPos
            self.yPos = (math.sin(self.angle)*self.ySpeed)+self.yPos

        return True

    def handleCollisions(self):
        if self.collisions:
            for c in self.collisions:
                if isinstance(c.entity, Ground):
                    self.explode()

    def explode(self):
        self.isExploding = True
        self.isFlaggedForDel = True

class Rocket(Projectile):
    def __init__(self, x, y, angle):
        Projectile.__init__(self,x,y,angle, 12, 8)
        self.image = load("../res/img/entities/testing/rocket.png")
        self.sprite = Sprite(self.image, self.xPos, self.yPos)
        self.sprite.rotation = 360 - math.degrees(angle)

        self.speed = 5
        self.velocity = 0.7
        self.maxSpeed = 20

        self.xSpeed = self.speed
        self.xVelocity = self.velocity
        self.maxXSpeed = self.maxSpeed

        self.ySpeed = self.speed
        self.yVelocity = self.velocity
        self.maxYSpeed = self.maxSpeed


##########Explosions##########
class Explosion(GameEntity):
    def __init__(self, x, y, tick, flipped, xOffset, yOffset):
        GameEntity.__init__(self,x,y,97,117)
        self.isCollidable = False
        self.isUpdatable = True
        self.image = load("../res/img/droid/FX.png")
        self.sheet = ImageGrid(self.image, 1,4,97,117,0,0)
        self.texture = TextureGrid(self.sheet)
        self.tick = tick
        self.life = 3
        self.end = tick + self.life
        self.force = self.life

        self.animEnd = self.life
        self.animFrame = 0
        self.flipped = flipped

        self.xOffset = xOffset
        self.yOffset = yOffset


    def customUpdate(self):
        self.animate()

        if self.tick == self.end:
            self.isFlaggedForDel = True

        self.tick += 1
        self.animFrame += 1
        self.force -= 1

    def animate(self):
        img = self.texture.get(0,self.animFrame)

        img = img.get_transform(self.flipped, False, 0)

        self.sprite.image = img
        self.sprite.x = self.xCam + self.xOffset
        self.sprite.y = self.yCam + self.yOffset


        """if self.isJumping and self.jumpAnimCount < len(self.texture):
            img = self.texture.get(0,self.jumpAnimCount)
            self.jumpAnimCount += 1
        else:
            img = self.texture.get(0,0)
            self.jumpAnimCount = 0

        if self.jumpAnimCount == len(self.texture) - 1:
            self.atShootingFrame = True
        else:
            self.atShootingFrame = False

        img = img.get_transform(self.facingLeft, False, 0)

        self.sprite.image = img

        self.updateSprite()"""

##########Surfaces##########

class Ground(GameEntity):
    def __init__(self,x, y):
        GameEntity.__init__(self,x, y, 32,32)
        self.isCollidable = True
        self.isUpdatable = True
        self.image = load("../res/img/fg/ground/ground_tile.png")

class GroundGrid(Ground):
    def __init__(self,x, y, columns, rows, tileSize):
        GameEntity.__init__(self,x, y, 32,32)
        self.isCollidable = True
        self.isUpdatable = True
        self.image = load("../res/img/fg/ground/ground_tile.png")
        self.col = columns
        self.row = rows
        self.ground = None
        self.tileSize = tileSize
        self.width = columns * tileSize
        self.height = rows * tileSize

        self.setup()

    def setup(self):
        self.ground = []

        for i in range(self.col):
            col = []
            for j in range(self.row):
                col.append(Ground(self.xPos + i * self.tileSize, self.yPos + j * self.tileSize))
            self.ground.append(col)

    def createSprite(self):
        for col in range(self.col):
            for row in range(self.row):
                self.ground[col][row].sprite = Sprite(self.ground[col][row].image, self.ground[col][row].xCam, self.ground[col][row].yCam)

    def update(self, camera, batch, group, player):
        inCam = self.isInCamera(camera)

        if inCam:
            self.getRelCamCoords(camera.xPos, camera.yPos)


        for col in range(self.col):
            for row in range(self.row):
                inCam = self.ground[col][row].isInCamera(camera)

                if inCam:
                    self.ground[col][row].getRelCamCoords(camera.xPos, camera.yPos)
                    if self.ground[col][row].sprite is None:
                        self.ground[col][row].createSprite()
                    else:
                        self.ground[col][row].updateSprite()
                    self.ground[col][row].sprite.batch = batch
                    self.ground[col][row].sprite.group = group

                elif not inCam and self.ground[col][row].sprite is not None:
                    self.ground[col][row].sprite.batch = None
                    self.ground[col][row].sprite.group = None

        if self.isHittable:
            self.checkPlayerPos(player)

            if self.isHit:
                self.onHit()

        self.customUpdate()

        if self.isCollidable:
            self.updateAABB()

    def updateSprite(self):
        pass


class Island(GameEntity):
    def __init__(self,x, y):
        GameEntity.__init__(self,x, y, 360,71)
        self.isCollidable = True
        self.isUpdatable = True
        self.image = load("../res/img/fg/island/island.png")
        self.aabb = {
            "x": self.xPos,
            "y": self.yPos + 49,
            "width": self.width,
            "height": 20
        }


##########Utilities##########

class EndOfLevel(GameEntity):
    def __init__(self, x, y):
        GameEntity.__init__(self,x,y,127,78)
        self.isCollidable = False
        self.isHittable = True
        self.image = load("../res/img/utils/arrow/arrow.png")
        self.sheet = ImageGrid(self.image, 4,4,127,78,0,0) ##image, rows, columns, item_width=None, item_height=None, row_padding=0, column_padding=0
        self.texture = TextureGrid(self.sheet)
        self.animRow = 3  #for some reason the import is bad. the sprite sheet is in reverse???????
        self.animCol = 3
        self.animFrame = 0
        self.animEnd = 14

    def customUpdate(self):
        self.animate()

    def animate(self):
        if self.animFrame < self.animEnd:
            #self.sprite.image = self.texture.get(self.animRow, self.animCol)
            self.animFrame += 1

            if self.animCol <= 3:
                self.animCol += 1

        else:
            self.animFrame = 0

    def createSprite(self):
        self.sprite = Sprite(self.texture.get(0,0), self.xCam, self.yCam)

    def checkPlayerPos(self, player):
        if player.xPos + player.width > self.xPos and player.xPos < self.xPos + self.width and player.yPos < self.yPos + self.height and player.yPos + player.height > self.yPos:
            self.isHit = True
      
##########Background Elements##########

class BackgroundEntity(GameEntity):
    def __init__(self,x,y,width,height):
        GameEntity.__init__(self,x,y, width, height)
        self.isCollidable = False

class Cloud1(BackgroundEntity):
    def __init__(self,x,y):
        BackgroundEntity.__init__(self,x,y,957,430)
        self.image = load("../res/img/bg/BGgr_cloud.png")

class Cloud2(BackgroundEntity):
    def __init__(self,x,y):
        BackgroundEntity.__init__(self,x,y,1026,511)
        self.image = load("../res/img/bg/BGgr_cloud2.png")

class Sun(BackgroundEntity):
    def __init__(self,x,y):
        BackgroundEntity.__init__(self,x,y,819,407)
        self.image = load("../res/img/bg/BGgr_sun.png")

class TreeBack1(BackgroundEntity):
    def __init__(self, x, y):
        BackgroundEntity.__init__(self, x, y, 269,205)
        self.image = load("../res/img/bg/BGgr_treesback1.png")

class TreeBack2(BackgroundEntity):
    def __init__(self, x, y):
        BackgroundEntity.__init__(self, x, y, 288,234)
        self.image = load("../res/img/bg/BGgr_treesback2.png")

class TreeBack3(BackgroundEntity):
    def __init__(self, x, y):
        BackgroundEntity.__init__(self, x, y, 275,220)
        self.image = load("../res/img/bg/BGgr_treesback3.png")

class TreeFront1(BackgroundEntity):
    def __init__(self, x, y):
        BackgroundEntity.__init__(self, x, y, 615,466)
        self.image = load("../res/img/bg/BGgr_treesfront1.png")

class TreeFront2(BackgroundEntity):
    def __init__(self, x, y):
        BackgroundEntity.__init__(self, x, y, 497,363)
        self.image = load("../res/img/bg/BGgr_treesfront2.png")

class TreeFront3(BackgroundEntity):
    def __init__(self, x, y):
        BackgroundEntity.__init__(self, x, y, 632,451)
        self.image = load("../res/img/bg/BGgr_treesfront3.png")
