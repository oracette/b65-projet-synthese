from Map import Map
from Level import Level
from Entities import *

def build(tileSize):
        levels = []


##########################################################LEVEL 1#######################################################################

        width = 3200
        height = 1000

        #what is needed: foreground, ground, player, utils, background

        levelTileWidth = int(width / tileSize)
        levelTileHeight = int(height / tileSize)

        foreground = []
        ground = []
        player = Robot(12*tileSize, 3*tileSize)
        utils = []
        background = []

        #GROUND

        #New method for making maps, ground grids! essentially a group of ground entities that are considered one.
        #borders
        #down
        ground.append(GroundGrid(0*tileSize,0*tileSize,levelTileWidth,2,tileSize))
        #left
        ground.append(GroundGrid(0*tileSize,1*tileSize,3,levelTileHeight-1,tileSize))
        #right
        ground.append(GroundGrid((levelTileWidth - 4)*tileSize,0*tileSize,3,levelTileHeight-1,tileSize))
        #up
        ground.append(GroundGrid(0*tileSize,(levelTileHeight -2)*tileSize,levelTileWidth,2,tileSize))


        #ground.append(GroundGrid(20*tileSize,2*tileSize,3,15,tileSize))

        #ISLAND

        ground.append(Island(23*tileSize, 8*tileSize))
        ground.append(Island(47*tileSize, 14*tileSize))
        ground.append(Island(63*tileSize, 8*tileSize))

        #BACKGROUND OBJECTS

        #Clouds
        background.append(Cloud1(-300,0))

        background.append(Cloud2(1000,50))

        background.append(Cloud1(2300,0))


        #tree backs
        background.append(TreeBack3(400,0))
        background.append(TreeBack2(1000,0))
        background.append(TreeBack1(1350,0))
        background.append(TreeBack3(1700,0))
        background.append(TreeBack2(2200,0))
        background.append(TreeBack1(2500,0))
        background.append(TreeBack3(2900,0))

        #tree fronts
        background.append(TreeFront1(50,0))
        background.append(TreeFront2(700,0))
        background.append(TreeFront3(1300,0))
        background.append(TreeFront2(1900,0))
        background.append(TreeFront1(2600,0))

        #sun
        #background.append(Sun(500,500))


        utils.append(EndOfLevel(90*tileSize, 2*tileSize))


        camPos = (0,0)


        levels.append(Level(width, height, foreground, ground, player, utils, background, camPos))

##########################################################LEVEL 2#######################################################################

        width = 800
        height = 600

        levelTileWidth = int(width / tileSize)
        levelTileHeight = int(height / tileSize)

        foreground = []
        ground = []
        player = Robot(12*tileSize, 3*tileSize)
        utils = []
        background = []

        #GROUND

        #borders
        #down
        ground.append(GroundGrid(0*tileSize,0*tileSize,levelTileWidth,2,tileSize))
        #left
        ground.append(GroundGrid(0*tileSize,1*tileSize,3,levelTileHeight-1,tileSize))
        #right
        ground.append(GroundGrid((levelTileWidth - 3)*tileSize,0*tileSize,3,levelTileHeight-1,tileSize))
        #up
        ground.append(GroundGrid(0*tileSize,(levelTileHeight -2)*tileSize,levelTileWidth,2,tileSize))

        camPos = (0,0)

        levels.append(Level(width, height, foreground, ground, player, utils, background, camPos))


        map = Map("first level!",levels)

        return map
